package com.mycompany.gameofrides;


import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;



public class Car_models extends ActionBarActivity implements AdapterView.OnItemSelectedListener {


    public Spinner spinner2, spinner3, spinner, spinner4, spinner5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_models);


        spinner = (Spinner) findViewById(R.id.spinner);
        spinner2 = (Spinner) findViewById(R.id.spinner2);
        spinner3 = (Spinner) findViewById(R.id.spinner3);
        spinner4 = (Spinner) findViewById(R.id.spinner4);
        spinner5 = (Spinner) findViewById(R.id.spinner5);


        ArrayAdapter<CharSequence> staticAdapter = ArrayAdapter
                .createFromResource(this, R.array.kingdom,
                        android.R.layout.simple_spinner_item);

        staticAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(staticAdapter);

        ArrayAdapter<CharSequence> staticAdapter2 = ArrayAdapter
                .createFromResource(this, R.array.car_brand,
                        android.R.layout.simple_spinner_item);

        staticAdapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner2.setAdapter(staticAdapter2);


        ArrayAdapter<CharSequence> staticAdapter4 = ArrayAdapter
                .createFromResource(this, R.array.min_price,
                        android.R.layout.simple_spinner_item);

        staticAdapter4.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        spinner4.setAdapter(staticAdapter4);

        ArrayAdapter<CharSequence> staticAdapter5 = ArrayAdapter
                .createFromResource(this, R.array.max_price,
                        android.R.layout.simple_spinner_item);

        staticAdapter5.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner5.setAdapter(staticAdapter5);
        spinner2.setOnItemSelectedListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_car_models, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    public void onItemSelected(AdapterView<?> parent, View v, int pos, long id) {

        String str =  (String) parent.getItemAtPosition(pos);


        if (str.equals("Alfa Romeo")) {

            ArrayAdapter<CharSequence> staticAdapter3 = ArrayAdapter
                    .createFromResource(this, R.array.alfa_model,
                            android.R.layout.simple_spinner_item);

            staticAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinner3.setAdapter(staticAdapter3);

        }
        if (str.equals("Mitsubishi")) {

            ArrayAdapter<CharSequence> staticAdapter3 = ArrayAdapter
                    .createFromResource(this, R.array.mitsu_model,
                            android.R.layout.simple_spinner_item);

            staticAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinner3.setAdapter(staticAdapter3);

        }
        if (str.equals("Chevrolet")) {

            ArrayAdapter<CharSequence> staticAdapter3 = ArrayAdapter
                    .createFromResource(this, R.array.cher_model,
                            android.R.layout.simple_spinner_item);

            staticAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinner3.setAdapter(staticAdapter3);

        }
        if (str.equals("Volkswagen")) {

            ArrayAdapter<CharSequence> staticAdapter3 = ArrayAdapter
                    .createFromResource(this, R.array.volk_model,
                            android.R.layout.simple_spinner_item);

            staticAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinner3.setAdapter(staticAdapter3);

        }
        if (str.equals("BMW")) {

            ArrayAdapter<CharSequence> staticAdapter3 = ArrayAdapter
                    .createFromResource(this, R.array.bmw_model,
                            android.R.layout.simple_spinner_item);

            staticAdapter3.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinner3.setAdapter(staticAdapter3);

        }
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    public void submit(View view) {

        Intent intent = new Intent(this, Car_rates.class);

        String spin1 = spinner.getSelectedItem().toString();
        String spin2 = spinner2.getSelectedItem().toString();
        String spin3 = spinner3.getSelectedItem().toString();
        String spin4 = spinner4.getSelectedItem().toString();
        String spin5 = spinner5.getSelectedItem().toString();


        Bundle b = new Bundle();
        b.putString("kingdom", spin1);
        b.putString("brand", spin2);
        b.putString("model", spin3);
        b.putString("min_coins", spin4);
        b.putString("max_coins", spin5);
        intent.putExtras(b);
        startActivity(intent);


    }


}