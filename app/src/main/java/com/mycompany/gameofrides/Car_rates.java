package com.mycompany.gameofrides;

import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;


public class Car_rates extends ActionBarActivity {

    int max_array = 7; //maximum array size

   // the following are the rates of the car models in ascending order

    public int[] space = {200, 250, 325, 450, 470, 500, 525};
    public int[] attrage = {250, 370, 450, 525, 600, 675, 790};
    public int[] outlander = {150, 325, 450, 500, 525, 650, 800};
    public int[] mirage = {250, 325, 470, 500, 525, 600, 880};
    public int[] lancer= {250, 325, 450, 470, 660, 750, 910};

    public int[] cc= {200, 250, 325, 450, 470, 500, 525};
    public int[] cs= {175, 300, 550, 600, 650, 680, 780};
    public int[] gt= {200, 250, 300, 450, 550, 600, 840};
    public int[] brera= {200, 225, 300, 450, 450, 600, 750};
    public int[] one= {175, 200, 450, 550, 650, 700, 825};

    public int[] agile= {100, 225, 325, 450, 570, 800, 925};
    public int[] aveo= {200, 250, 325, 550, 670, 700, 825};
    public int[] ss= {300, 450, 470, 500, 675, 800, 950};
    public int[] impala= {200, 250, 325, 450, 470, 500, 525};
    public int[] spark= {200, 250, 525, 650, 770, 800, 825};

    public int[] m= {200, 375, 450, 600, 650, 700, 800};
    public int[] s= {200, 250, 325, 450, 470, 500, 525};
    public int[] x= {200, 250, 325, 450, 470, 500, 525};
    public int[] z= {200, 250, 325, 450, 470, 500, 525};
    public int[] bmwx= {200, 250, 325, 450, 470, 500, 525};

    public int[] polo= {200, 250, 275, 400, 450, 560, 600};
    public int[] vento= {220, 310, 375,440, 520, 600, 750};
    public int[] jetta= {200, 250, 325, 450, 470, 500, 525};
    public int[] passat= {200, 250, 325, 450, 470, 500, 525};
    public int[] beetle= {150, 200, 325, 400, 470, 500, 525};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_car_rates);

        Intent i = getIntent();
        String kingdom_name = i.getStringExtra("kingdom");
        String brand_name = i.getStringExtra("brand");
        String model_no = i.getStringExtra("model");
        String min_amount = i.getStringExtra("min_coins");
        String max_amount = i.getStringExtra("max_coins");

        int min =Integer.valueOf(min_amount);
        int max = Integer.valueOf(max_amount);

        int [] rate = check_model(min, max, model_no);
        display(kingdom_name, brand_name, model_no, rate );


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_car_rates, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public int [] check_model(int min, int max, String mod) {
        int [] rate = new int[3];

        switch(mod) {

            case "8C Competizione":rate = threeRates(min, max,cc);
                break;
            case "8C Spider": rate = threeRates(min, max,cs);
                break;
            case "GT":rate =  threeRates(min, max,gt);
                break;
            case "Brera": rate = threeRates(min, max,brera);
                break;
            case "159":rate  = threeRates(min, max,one);
                break;
            case "Space Star": rate = threeRates(min, max,space);
                break;
            case "Attrage": rate = threeRates(min, max,attrage);
                break;
            case "Outlander":rate =  threeRates(min, max,outlander);
                break;
            case "Mirage": rate = threeRates(min, max,mirage);
                break;
            case "Lancer Hatchback": rate = threeRates(min, max,lancer);
                break;
            case "Agile": rate = threeRates(min, max,agile);
                break;
            case "Aveo": rate = threeRates(min, max,aveo);
                break;
            case "SS": rate = threeRates(min, max,ss);
                break;
            case "Impala": rate = threeRates(min, max,impala);
                break;
            case "Spark":rate =  threeRates(min, max,spark);
                break;
            case "Polo": rate = threeRates(min, max,polo);
                break;
            case "Vento":rate =  threeRates(min, max,vento);
                break;
            case "Jetta": rate =  threeRates(min, max,jetta);
                break;
            case "Passat": rate = threeRates(min, max,passat);
                break;
            case "Beetle": rate = threeRates(min, max,beetle);
                break;
            case "M3": rate = threeRates(min, max,m);
                break;
            case "Series 3": rate = threeRates(min, max,s);
                break;
            case "X3": rate = threeRates(min, max,x);
                break;
            case "Z4": rate = threeRates(min, max,z);
                break;
            case "X5": rate = threeRates(min, max,bmwx);
                break;
            default:

        }
        return rate;

    }

   public int [] threeRates(int min, int max, int[] array) {

        int k = 0;
        int [] rate = new int[3]; // array with required rates

        for(int i = 0; i < max_array; i++) {
            if((array[i] >= min)&&(array[i] <= max)) {
                rate[k] = array[i];
                k++;
            }

            if(k==3) break;
        }
           return rate;


    }

    public void display(String kingdom_name, String brand_name, String model_no, int [] rate) {


        StringBuilder sb1 = new StringBuilder();  // converting integer into string
        sb1.append(rate[0]);
        String str1 = sb1.toString();

        StringBuilder sb2 = new StringBuilder();
        sb2.append(rate[1]);
        String str2 = sb2.toString();

        StringBuilder sb3 = new StringBuilder();
        sb3.append(rate[2]);
        String str3 = sb3.toString();

        String [] str = new String[3];
        str[0]=str1;
        str[1]=str2;
        str[2]=str3;


        // Create the text view
       ListView list = (ListView) findViewById(R.id.listView);

        ArrayAdapter<String> adapter = new ArrayAdapter(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, str);
        list.setAdapter(adapter);

        TextView myTextView = (TextView) findViewById(R.id.viewkingdom);
        myTextView.setText(kingdom_name);

        TextView myTextView1 = (TextView) findViewById(R.id.viewcarbrand);
        myTextView1.setText(brand_name);

        TextView myTextView2 = (TextView) findViewById(R.id.viewcarmodel);
        myTextView2.setText(model_no);

    }

}
